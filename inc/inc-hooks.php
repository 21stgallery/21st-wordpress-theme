<?php 

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

function my_theme_enqueue_styles() {     
    
  $path_js = get_stylesheet_directory_uri() . '/assets/js/';
 	$path_css = get_stylesheet_directory_uri() . '/assets/css/';
            		
	wp_deregister_script( 'wp-embed' );
	 	
	// wp_localize_script( 'jquery', 'siteVars', [] );						
  // wp_enqueue_script('jquery-mask', '//cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js', ['jquery'], false, true);
  wp_enqueue_script('flexslider', '//cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.2/jquery.flexslider.min.js', ['jquery'], false, true);
  wp_enqueue_script('fancybox', '//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js', ['jquery'], false, true);

	/* Choices JS */
	/* ----------------------------------------- */
		// wp_enqueue_script('choices-js', '//cdn.jsdelivr.net/npm/choices.js@4.1.0/public/assets/scripts/choices.min.js', ['jquery'], false, true);
		// wp_enqueue_style( 'choices-css', '//cdn.jsdelivr.net/npm/choices.js@4.1.0/public/assets/styles/choices.min.css');
	/* ----------------------------------------- Choices JS */
		
	/* Caleran JS */
	/* ----------------------------------------- */
		// wp_enqueue_script('moment-js', '//cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js', ['jquery'], false , true);
		// wp_enqueue_script('momentjs-ptbr', '//cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/locale/pt-br.js', ['moment-js'], '2.20.1', true );
		// wp_enqueue_script('caleran-js', $path_js.'caleran.min.js', ['jquery', 'moment-js'], false , true);
		// wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
	/* ----------------------------------------- Caleran JS */
	
	/* Scroll Magic */
	/* ----------------------------------------- */
		// wp_enqueue_script('scrollmagic', '//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js', [], false, true);				
		// wp_enqueue_script('scrollmagic--debug', '//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js', ['scrollmagic'], false, true);
	/* ----------------------------------------- Scroll Magic */
	

	/* GSAP */
	/* ----------------------------------------- */
		// wp_enqueue_script('gsap', '//cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenMax.min.js', ['jquery'], false, true);		
		// wp_enqueue_script('gsap-css-plugin', '//cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/plugins/CSSPlugin.min.js', [], false, true);				
		// wp_enqueue_script('gsap-css-rule-plugin', '//cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/plugins/CSSRulePlugin.min.js', [], false, true);				
		// wp_enqueue_script('scrollmagic--gsap', '//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.min.js', [], false, true);		
	/* ----------------------------------------- GSAP */
	
	/* Sticky */
	/* ----------------------------------------- */
		wp_enqueue_script('stickybits', $path_js . 'vendors/stickybits.min.js', ['jquery'], false, true);
	/* ----------------------------------------- Sticky */
	
	
	/* Custons */
	/* ----------------------------------------- */
		wp_enqueue_script('app', $path_js . 'app-min.js', ['jquery'], false, true);		
		wp_enqueue_style( 'fancybox', '//cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css');
		// wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=');
		// wp_enqueue_style( 'bootstrap', $path_css . 'bootstrap/bootstrap.css');
	  wp_enqueue_style( 'main', $path_css . 'styles.css');
		
	/* ----------------------------------------- Custons */	
}


add_action( 'pre_get_posts', 'set_query_for_archive_artists' );
function set_query_for_archive_artists($query){
    if ( 
				is_post_type_archive('artist') && 
				$query->is_main_query() &&
				!$query->is_admin
			) {
				$query->set( 'orderby', 'meta_value' );
				$query->set( 'meta_key', 'last_name' );
				$query->set( 'order', 'ASC' );
				$query->set( 'posts_per_page', 100 );

			return $query;
		}
}



// Add the custom columns to the work post type:
add_filter( 'manage_work_posts_columns', 'set_custom_edit_work_columns' );
function set_custom_edit_work_columns($columns) {
    unset( $columns['author'] );
    $columns['artist'] = __( 'Artist', '21stgallery' );
    $columns['thumbnail'] = __( 'Thumbnail', '21stgallery' );

    return $columns;
}

// Add the data to the custom columns for the work post type:
add_action( 'manage_work_posts_custom_column' , 'custom_work_column', 10, 2 );
function custom_work_column( $column, $post_id ) {
    switch ( $column ) {

        case 'artist' :
						$artist_id = get_field('artist_id', $post_id);
						echo get_the_title($artist_id);
					break;
					
				case 'thumbnail' :
						$images = get_field('images', $post_id);
						if ($images && count($images) > 0) {
							echo '<img src="'.$images[0]['sizes']['thumbnail'].'" />';
						}
            break;

    }
}


// Add the custom columns to the artist post type:
add_filter( 'manage_artist_posts_columns', 'set_custom_edit_artist_columns' );
function set_custom_edit_artist_columns($columns) {
	unset($columns['post_type']);
	$columns['first_name'] = __( 'First Name', '21stgallery' );
	$columns['last_name'] = __( 'Last Name', '21stgallery' );	
	return $columns;
}

// Add the data to the custom columns for the artist post type:
add_action( 'manage_artist_posts_custom_column' , 'custom_artist_column', 10, 2 );
function custom_artist_column( $column, $post_id ) {
	switch ( $column ) {
		case 'first_name' :
			echo get_field('first_name', $artist_id);
		break;
		
		case 'last_name' :
			echo get_field('last_name', $artist_id);
		break;
	}
}

function admin_remove_default_post_menu ()
{ 
   remove_menu_page('edit.php');
}

add_action('admin_menu', 'admin_remove_default_post_menu'); 




add_filter( 'apto_reorder_item_thumbnail', 'apto_change_image_thumbnail', 10, 2);
function apto_change_image_thumbnail($image_html, $post_id)
{
	$gallery = get_post_meta( $post_id, 'images', true );
	$image_id = $gallery && count($gallery) >  0 ? $gallery[0] : false;

	$image = wp_get_attachment_image_src( $image_id , array(150,150));	

	return $image !== FALSE ? '<img src="'. $image[0] .'" alt="" />' : $image_html;
}


add_filter('post_type_link', 'cj_update_permalink_structure', 10, 2);
function cj_update_permalink_structure( $post_link, $post )
{	
	if ( false !== strpos( $post_link, '%groups%' ) ) {
		$taxonomy_terms = get_the_terms( $post->ID, 'groups' );				
		if ($taxonomy_terms && count($taxonomy_terms) > 0) {
			foreach ( $taxonomy_terms as $term ) { 
				if ( ! $term->parent ) {
						$post_link = str_replace( '%groups%', $term->slug, $post_link );
				}
			}
		} 
	}
	return $post_link;
}


// initial hook
add_action( 'save_post', 'wpse105926_save_post_callback' );
function wpse105926_save_post_callback( $post_id ) {

    // verify post is not a revision
    if ( ! wp_is_post_revision( $post_id ) ) {

        // unhook this function to prevent infinite looping
        remove_action( 'save_post', 'wpse105926_save_post_callback' );

				$artist_id = get_post_meta($post_id, 'artist_id', true);
				if ($artist_id) {
					
					$post_name = get_the_title( $artist_id ) . ' ' . get_the_title( $post_id );
	
					// update the post slug
					wp_update_post( array(
							'ID' => $post_id,
							'post_name' => $post_name // do your thing here
					));
					
				}

        // re-hook this function
        add_action( 'save_post', 'wpse105926_save_post_callback' );

    }
}
