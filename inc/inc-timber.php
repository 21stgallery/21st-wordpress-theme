<?php

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php') ) . '</a></p></div>';
	});

	add_filter('template_include', function($template) {
		return get_stylesheet_directory() . '/static/no-timber.html';
	});

	return;
}

Timber::$dirname = array('templates', 'views');

class StarterSite extends TimberSite {

	function __construct() {

		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'get_twig', array( $this, 'add_to_twig' ) );

		// add_filter('acf/settings/show_admin', array($this,'__return_false'));

		add_action('wp_enqueue_scripts', array($this, 'load_scripts'));
		add_action('wp_enqueue_scripts', array($this, 'load_styles'));

		add_action('init', array($this,  'removeHeadLinks'));

		parent::__construct();
	}


	function add_to_context( $context ) {
		$context['menu'] = new TimberMenu();
		$context['site'] = $this;
		return $context;
	}

	function add_to_twig( $twig ) {
		/* this is where you can add your own functions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );
		$twig->addFilter('myfoo', new Twig_SimpleFilter('myfoo', array($this, 'myfoo')));
		return $twig;
	}
	function load_scripts(){
		wp_enqueue_script('jquery');
		wp_enqueue_script( 'modernizr', THEME_URL . '/static/modernizr.custom.js', array('jquery'), null, false);
		// wp_enqueue_script( 'main', THEME_URL . '/static/main-min.js', array('jquery'), null, true);
	}

	function removeHeadLinks() {
		remove_action('wp_head', 'rsd_link');
		remove_action('wp_head', 'wlwmanifest_link');
		remove_action('wp_head', 'wp_generator');
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
	}

}

new StarterSite();


add_filter( 'timber_context', 'mytheme_timber_context'  );

function mytheme_timber_context( $context ) {
    $context['options'] = get_fields('option');
    return $context;
}

add_filter( 'timber/twig', 'add_to_twig' );

/**
 * Adds functionality to Twig.
 * 
 * @param \Twig\Environment $twig The Twig environment.
 * @return \Twig\Environment
 */
function add_to_twig( $twig ) {
    // Adding a function.
    $twig->addFilter( new Timber\Twig_Filter( 'ucwords', 'myucwords' ) );
    $twig->addFilter( new Timber\Twig_Filter( 'truncate', 'twig_truncate' ) );
    
    // // Adding functions as filters.
    // $twig->addFilter( new Timber\Twig_Filter( 'whateverify', 'whateverify' ) );
    // $twig->addFilter( new Timber\Twig_Filter( 'slugify', function( $title ) {
    //     return sanitize_title( $title );
    // } ) );
    
    return $twig;
}

function myucwords( $text ) {    
    return ucwords(strtolower($text));
}

function twig_truncate( $text ) {
	return strlen($text) > 50 ? substr($text,0,50)."..." : $text;
}
