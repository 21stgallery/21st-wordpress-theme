<?php

function pp_debug($var, $die = true) {
  echo '<pre>'.print_r($var,1). '</pre>';
  if ($die) {
    die();
  }
}

/**
 * Get All Archive Pages related to a WORK
 *
 * @return false | array
 */
function get_work_collections($work = false) {
  
  if (!$work) {
    global $post;
    $work = $post;
  }

  $return = [];

  // Get Artist Page Archive
  $artist_id = $post->artist_id;
  if ($artist_id) {
    $return[] = [
      'title' => get_the_title($artist_id),
      'url' => get_permalink($artist_id)
    ];
  }
  
  // Get The ArtWork Type
  $artwork_types = wp_get_post_terms($post->ID, 'artwork_type');
  if ($artwork_types) {
    $groups = get_work_groups_from_work_types($artwork_types);

    if ($groups) {
      foreach ($groups as $group) {
        $return[] = [
          'title' => $group->name,
          'url' => get_term_link($group)
        ];
      }
    }
  }  
  
  return $return;
}

/**
 * Return the Work Groups based on the Work Type
 *
 * @return void
 */
function get_work_groups_from_work_types($types) {

  $return = [];

  if ($types) {
    
    foreach ($types as $type ) {
      $groups = get_work_groups_from_work_type($type);
      
      if ($groups) {
        foreach ($groups as $group) {
          $return[] = $group;
        }
      }
    }

  }
  
  return $return;
}


/**
 * Return the list of works from an Artist
 *
 * @param [integer] $artist_id
 * @return Array
 */
function get_works_by_artist($artist_id) {
  
  $query = new Timber\PostQuery([
    'post_type' => 'work',
    'posts_per_page' => '-1',
    'meta_query' => [
      [
        'key' => 'artist_id',
        'value' => $artist_id,
        'compare' => 'LIKE'
      ]
    ]
  ]);

  return $query;

}


function wp_query_filter_by_term_order($orderby_statement, $query) {
  
  $tax_queries = isset($query->query_vars['tax_query']) ? $query->query_vars['tax_query'] : false;
  
  if ($tax_queries) {
    $terms = [];
    foreach ($tax_queries as $query) {
      if (isset($query['field']) && $query['field'] == 'term_id' && count($query['terms']) > 0) {
        foreach ($query['terms'] as $term) {
          $terms[] = $term;
        }
      }
    }
    
    if (count($terms) > 0) {
      $orderby_statement = "FIELD(term_taxonomy_id, ". implode(', ', $terms).")"; //keeps the current orderby, but also adds the term_order in front
    }
  }

  return $orderby_statement;
}

/**
 * Return the list of works from an Artist ordering by groups
 *
 * @param [integer] $artist_id
 * @return Array
 */
function get_works_by_artist_ordering_by_groups($artist_id) {
  
  //add my filter to change the order by of the query done by WP_QUERY:
  add_filter('posts_orderby', 'wp_query_filter_by_term_order', 999 , 2);

  $terms = get_field('artist_featured_artworks_order_by_artwork_groups', 'option');

  if (!$terms) {
    $terms = get_terms(array(
      'fields' => 'ids',
      'taxonomy'     => 'groups',
      'orderby'      => 'term_order',
      'hide_empty'   => true,
    ));
  }

  $query = new Timber\PostQuery([
    'post_type' => 'work',
    'posts_per_page' => -1,
    'update_post_meta_cache' => false,
    'update_post_term_cache' => false,
    'ignore_sticky_posts' => true,
    'no_found_rows' => true,
    'meta_query' => [
      [
        'key' => 'artist_id',
        'value' => $artist_id,
        'compare' => 'LIKE'
      ]
    ],
    'tax_query' => [
      [
        'taxonomy' => 'groups',
        'field' => 'term_id',
        'terms' => $terms
      ]
    ]
  ]);

  //after i am done, i remove the filter:
  remove_filter('posts_orderby', 'wp_query_filter_by_term_order');

  return $query;

}



/**
 * Return the list of Exhibitions and Fairs from an Artist
 *
 * @param [integer] $artist_id
 * @return Array
 */
function get_events_by_artist($artist_id) {
  
  $query = new Timber\PostQuery([
    'post_type' => ['exhibition', 'fair'],
    'posts_per_page' => '-1',
    'meta_query' => [
      [
        'key' => 'featured_artists',
        'value' => '"' . $artist_id . '"',
        'compare' => 'LIKE'
      ]
    ]
  ]);

  return $query;

}



/**
 * Return the list of Press from an Artist
 *
 * @param [integer] $artist_id
 * @return Array
 */
function get_press_by_artist($artist_id) {
  
  $query = new Timber\PostQuery([
    'post_type' => ['press_file'],
    'posts_per_page' => '-1',
    'meta_query' => [
      [
        'key' => 'related_artists',
        'value' => '"' . $artist_id . '"',
        'compare' => 'LIKE'
      ]
    ]
  ]);

  return $query;

}





function get_work_groups_from_work_type($type) {
  
  $query = new WP_Term_Query([
    'taxonomy' => 'groups',
    'hide_empty' => false,
    'meta_query' => [
      [
        'key' => 'artwork_types',
        'value' => $type->term_id,
        'compare' => 'LIKE'
      ]
    ]
  ]);

  return $query->get_terms();
}



function get_work_groups() {
  
  $query = new WP_Term_Query([
    'taxonomy' => 'groups',
    'hide_empty' => false,
    'parent' => 0
  ]);

  return array_map(function($group) {
    return new Timber\Term($group->term_id);
  }, $query->get_terms());
  
}


function get_term_childrens($term) {

  $query = new WP_Term_Query([
    'taxonomy' => $term->taxonomy,
    'hide_empty' => false,
    'parent' => $term->term_id
  ]);

  return array_map(function($group) {
    return new Timber\Term($group->term_id);
  }, $query->get_terms());

}


function get_artist_collection_by_last_name() {
  return new Timber\PostQuery([
    'post_type' => 'artist',
    'posts_per_page' => -1,
    'orderby'  => 'meta_value',
    'meta_key' => 'last_name',
    'order'		 => 'ASC'
  ]);  
}


function get_related_works($post_id) {

  // Get the related works by same category
  $terms = wp_get_post_terms($post_id, 'groups', [ 'fields' => 'ids' ]);

  if ($terms && count($terms) > 0) {

    return new Timber\PostQuery([
      'post_type' => 'work',
      'posts_per_page' => 8,
      'tax_query' => [
        [
          'taxonomy' => 'groups',
          'terms'    => $terms
        ]
      ]
    ]);

  } else {
    return false;
  }
  
  // echo '<pre>'.print_r($terms,1). '</pre>';
  // die();

}

