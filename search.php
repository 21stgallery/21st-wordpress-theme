<?php
/**
 * Search results page
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

 global $wp_query;

$templates = array( 'search.twig', 'archive.twig', 'index.twig' );

$context          = Timber::get_context();
$context['title'] = 'Search results for ' . get_search_query();
$context['posts'] = new Timber\PostQuery();
$context['search'] = [
  'results_count' => $wp_query->found_posts,
  'terms' => get_search_query()
];
Timber::render( $templates, $context );
