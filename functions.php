<?php

define('THEME_URL', get_template_directory_uri());

include_once('inc/inc-helpers.php');
include_once('inc/inc-timber.php');
include_once('inc/inc-hooks.php');
include_once('inc/inc-admin.php');


// add_action('init', 'my_change_products_url');

function my_change_products_url() {  
  if ( is_admin() ) {
      return;
  }
  $args = ['post_type' => 'work', 'posts_per_page' => -1 ];
  $query = new WP_Query($args); 
  if ($query->have_posts()):
    $count = 0;
    while ($query->have_posts()): $query->the_post();

    $title = get_the_title(get_the_ID());
    $update = wp_update_post([
      'ID' => get_the_ID(),
      'post_title' => $title
    ], true);

    endwhile;
  endif; wp_reset_postdata();
}
