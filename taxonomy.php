<?php
/**
 * The template for Taxonomy
 *
 * Methods for TimberHelper can be found in the /functions sub-directory
 *
 * @package 	WordPress
 * @subpackage 	Timber
 * @since 		Timber 0.1
 */

$context = Timber::get_context();

$tax_slug = get_query_var( 'taxonomy' );
$term_slug = get_query_var( 'term' );


$term = get_term_by( 'slug', $term_slug , $tax_slug );

$parent = $term->parent ? new Timber\Term($term->parent) : new Timber\Term($term->term_id);
$context['parent'] = $parent;

$context['childrens'] = get_term_childrens($parent);
$context['term'] = $term;
$context['title'] = $term->name;

if ($tax_slug == 'groups') {
  $context['global_menu_active'] = 'Works';
}

$templates = array('taxonomy.twig', 'archive.twig', 'index.twig');
array_unshift( $templates, 'taxonomy-' . $tax_slug . '.twig' );
Timber::render($templates, $context);
