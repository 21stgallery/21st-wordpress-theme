// @codekit-prepend './vendors/classie.js'
// @codekit-prepend './vendors/overlay.js'
/* app.js */
(function($) {

	var $window    = $(window),
			colorChanging = $('.colored'),
			$sidebar   = $(".side"),
			offset     = $sidebar.offset(),
			topPadding = 115,
			slider = $('.slider'),
			slides = $('.slide'),
			sliderWidth = 0,
			sticky = stickybits('.sticky', { stickyBitStickyOffset: 150 });

	function resize(){
		sticky.update();
		updateSlides();
		resizeSliderHome();
		var windowHeight = $(window).height();
		$("#home-feature").css({ height: windowHeight - 90 });
		$('#featured').css({ marginTop: windowHeight - ( windowHeight*4/100 ) }); // Tob Bar - Bottom border
	}

	function updateSlides() {

		$.each(slides, function(){
			var img = $(this).find('img');
			var width = img.width();
			var final_width = (width - ( width * 1.6666666 / 100 ));
		  sliderWidth += final_width;
		});
		

		// slider.css('width',sliderWidth);
		slider.detach();

		$('#SliderContainer').append(slider);
		for (var i = 0, l = slides.length; i<l; i++){
			var sliderOffset = $('.slider').offset() ? $('.slider').offset().left : 0;
			var offSet = $(slides[i]).offset().left + sliderOffset * -1;
			$(slides[i]).attr('data-offset', offSet);
		}
		
	}

	function scrollToImage(distance){
		$('#SliderContainer .slider').stop().animate({ scrollLeft:distance }, 500);
	}

	function fadeInSlider() {
		var slide = $('.slide');
		slide.find('img').css({'opacity': '1.0'});
		$('#SliderContainer').animate({opacity:1.0},500);
	}

	function updateLastArtist($el) {
		$('.artists-list li').removeClass('active');
		$el.addClass('active');
	}
	function resizeSliderHome() {

		var diffH = $("#feature_image").height() - $sidebar.height();

		$('.loader').fadeOut(150);

		$window.scroll(function(){
			var scrolled = Math.max(0, $(window).scrollTop());
				//Follow sidebar
				if( ($sidebar.length) && ( $(window).width() > 550 ) && ( $(window).height() > $sidebar.height() ) ){
					if (scrolled >= offset.top) {
						if ( scrolled >= diffH ){
							$sidebar.stop().animate({
								marginTop: diffH
							});
						} else {
							$sidebar.stop().animate({
								marginTop: scrolled - offset.top + topPadding
							});
						}
					} else {
						$sidebar.stop().animate({
							marginTop: 0
						});
					}
				}
		});

		$('#home-feature .flexslider').flexslider({
				controlNav: true,
				manualControls:".control-nav li a",
				controlsContainer: ".container",
				directionNav: false,
				animation: "fade",
				slideshow: true,
				slideshowSpeed: 3000,
				animationSpeed: 500,
				useCSS:true,
				smoothHeight:true,
				start: function(){
					var logoColor = $('li.flex-active-slide #caption').attr('class');
					$('.flex-active-slide .link').clone().prependTo('.links');
					colorChanging.toggleClass(logoColor);
					// var n = slider.count - 1;
					// var html = [];
					// for (var i = 0; i < n; i++) {
					//   html.push('<li><a></a></li>');
					// }
					// $(".control-nav").append( html.join('') );
				},
				before: function(slider){
					var dir = slider.direction, nextObj;
					if (dir === "next") {
						nextObj = (slider.currentSlide === slider.last) ? 0 : slider.currentSlide + 1;
					} else {
						nextObj = (slider.currentSlide === 0) ? slider.last : slider.currentSlide - 1;
					}
					var nextSlide = slider.slides.eq(nextObj);
					var logoColor = $(nextSlide).find("#caption").attr('class');
					colorChanging.removeClass("dark white accent");
					colorChanging.addClass(logoColor);
				},
				after: function(){
					$('.links p.link').remove();
					$('.flex-active-slide .link').clone().prependTo('.links');
				},
		});

		$('.page .flexslider').flexslider({
					animation: "slide"
		});

	}

	jQuery(document).ready(function($) {		

		$('#SliderContainer .slide').on('click', function(e){
			var target = $(e.currentTarget);
			var targetOffset = target.attr('data-offset');
			scrollToImage(targetOffset);
		});

		$(window).load(function(){

			fadeInSlider();
			$(window).on('resize',resize);
			resize();

		});
				

		//Smooth scroll
		$('a.smoothy, .js-smoothy').click(function(e) {
			e.preventDefault();
			 var href = $.attr(this, 'href');
			 var topbarHeight = $('.top_bar').outerHeight() + 25;
			 
			 $('html, body').stop().animate({
					 scrollTop: $(href).offset().top - topbarHeight
			 }, 500, function () {
					 window.location.hash = href;
			 });
			 return false;
		});

		// ARTISTS LIST / THUMBS
		if ( $(".artists-list").length > 0 ) {

			$(".artists-list a")
				
				.hover(function(){
				
				updateLastArtist($(this).parent('li'));
				$(".artists-thumb img").remove();
				
				var thumb = $(this).data("thumb");
				if ( thumb !== undefined ) {
					if ( thumb.length > 0 ) {
						var image = $(`<img src="${thumb}" title="${$.trim($(this).text())}" />`).hide().fadeIn(700);
						$('.artists-thumb').append(image);
					}
				}
			}, function() {
				// $(".artists-thumb img").remove();
			});
		}

		//Toggle inquiries form
		$( "#displayText").click(function() {
			$(this).addClass('open');
			$( "#toggleText" ).slideToggle();
		});
	});
})( jQuery );
