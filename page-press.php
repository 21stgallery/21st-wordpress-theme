<?php
/**
* Template Name: Press
*/

global $paged;
if (!isset($paged) || !$paged){
    $paged = 1;
}

$context = Timber::get_context();
$context['title'] = get_the_title();

$args = [
  'post_type'   => 'press_file',
  'posts_per_page' => 60,
  'paged' => $paged
];
$context['posts'] = new Timber\PostQuery($args);
Timber::render( array( 'archive-press.twig', 'page.twig' ), $context );