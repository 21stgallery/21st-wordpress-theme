<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

$templates = array( 'archive.twig', 'index.twig' );

$context = Timber::get_context();


if ( is_tag() ) {
	$context['title'] = single_tag_title( '', false );

} else if ( is_category() ) {
	$context['title'] = single_cat_title( '', false );
	array_unshift( $templates, 'archive-' . get_query_var( 'cat' ) . '.twig' );

} else if ( is_post_type_archive() ) {
			
	$post_type = get_post_type_object(get_post_type());
	$file_slug = $post_type->name == 'exhibition' || $post_type->name == 'fair' ? 'events' : $post_type->name;

	array_unshift( $templates, 'archive-' . $file_slug . '.twig' );
	
	$context['title'] = $post_type->label;

	if ($post_type->name == 'exhibition' || $post_type->name == 'fair') {

		// $context['exhibition_date'] = Timber::get_terms('exhibition_date',
		// 	array(
		// 		'orderby' => 'term_order',
		// 		'hide_empty' => false,
		// 	)
		// );

		// $context['exhibition_time'] = Timber::get_terms('exhibition_time',
		// 	array(
		// 		'orderby' => 'term_order',
		// 		'hide_empty' => false,
		// 	)
		// );

		$args = array(
			'post_type'   => $post_type->name,
			'posts_per_page' => '-1',
			'orderby'     => 'post_date',
			'tax_query' => array(
					array(
							'taxonomy' => 'exhibition_time',
							'field'    => 'slug',
							'terms'    => 'current',
					),
			),
		);

		$context['current'] = Timber::get_posts($args);


		$args = array(
				'post_type'   => $post_type->name,
				'posts_per_page' => '-1',
				'orderby'     => 'post_date',
				'tax_query' => array(
						array(
								'taxonomy' => 'exhibition_time',
								'field'    => 'slug',
								'terms'    => 'past',
						),
				),
		);

		$context['past'] = Timber::get_posts($args);

	} else if( $post_type->name == 'work') {
		
		$context['groups'] = get_work_groups(); 

	}

}

$context['posts'] = new Timber\PostQuery();

Timber::render( $templates, $context );
