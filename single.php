<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;
$context['title'] = $post->post_title;

$templates = array( 'single-' . $post->ID . '.twig', 'single-' . $post->post_type . '.twig', 'single.twig' );

if ( post_password_required( $post->ID ) ) {
	Timber::render( 'single-password.twig', $context );
} else {
	
	if (
		$post->post_type == 'exhibition' ||
		$post->post_type == 'fair'
	) {

		$context['press'] = Timber::get_posts(array(
			'post_type' => 'press_file',
			'meta_query' => array(
				array(
					'key' => 'related_exhibition', // name of custom field
					'value' => '"' . get_the_ID() . '"', // matches exaclty "123", not just 123. This prevents a match for "1234"
					'compare' => 'LIKE'
				)
			)
		));	

		array_unshift( $templates, 'single-event.twig' );

	} else if ( $post->post_type == 'work' ) {		

		$context['collections'] = get_work_collections();	
		
		// $artist_name
		$artist_id = get_field('artist_id', get_the_ID(), false);		
		if ($artist_id) {
			$firstName = get_field('first_name', $artist_id);
			$lastName = get_field('last_name', $artist_id);
			
			if ($firstName && $lastName) {
				$context['artist_name'] = strtoupper($lastName) . ' ' . $firstName;
			} else {
				$context['artist_name'] = get_the_title( $artist_id );
			}
			
		}

		// $related_items
		$context['related'] = get_related_works($post->ID);
		
	} else if ( $post->post_type == 'artist' ) {		
		
		$context['isArtist'] = true;

		$artworks_ordering = get_field('featured_works_ordering') ?? 'term_order';
		if ($artworks_ordering == 'term_order') {
			$context['works'] = get_works_by_artist_ordering_by_groups($post->ID);
		} else {
			$context['works'] = get_works_by_artist($post->ID);
		}
		$context['events'] = get_events_by_artist($post->ID);
		$context['press'] = get_press_by_artist($post->ID);

	}

	Timber::render( $templates, $context, 5, Timber\Loader::CACHE_NONE );
}
